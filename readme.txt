Use at own risk.
Modified Version of Contact Form 7.

Stops files being deleted.
Stops randomised folder creation on upload.
Sets permissions on uploaded files so they are accessible externally.